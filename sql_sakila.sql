USE sakila;

SHOW TABLES;

DESCRIBE actor;

SELECT * FROM actor;

-- 1a
SELECT first_name, last_name
FROM actor;

-- 1b
SELECT CONCAT(first_name, ' ', last_name) AS 'Actor Name'
FROM actor;

-- 2a
SELECT actor_id, first_name, last_name
FROM actor 
WHERE first_name = 'Joe';

-- 2b
SELECT first_name, last_name
FROM actor
WHERE last_name LIKE '%GEN%';

-- 2c
SELECT last_name, first_name
FROM actor
WHERE last_name LIKE '%LI%'
ORDER BY last_name,first_name;

-- 2d
SELECT country_id, country
FROM country
WHERE country IN ('Afghanistan', 'Bangladesh', 'China');

-- 3a
ALTER TABLE actor
ADD middle_name VARCHAR(50) AFTER first_name;

-- 3b
ALTER TABLE actor
MODIFY COLUMN middle_name blob;

-- 3c
ALTER TABLE actor
DROP COLUMN middle_name;

-- 4a
SELECT last_name, COUNT(last_name) AS 'Last Name Frequency'
FROM actor
GROUP BY last_name;

-- 4b 
SELECT last_name, COUNT(last_name) AS 'Last Name Frequency'
FROM actor
GROUP BY last_name
HAVING `Last Name Frequency` >= 2;

-- 4c 
UPDATE actor
SET first_name = 'HARPO'
WHERE first_name = 'GROUCHO' AND last_name = 'WILLIAMS';

-- 4d

UPDATE actor
SET first_name =
CASE
	WHEN first_name = 'HARPO'
		THEN 'GROUCHO'
	ELSE 'MUCHO GROUCHO'
END
WHERE actor_id = 172;

-- 5a
SHOW CREATE TABLE address;

-- 6a
SELECT s.first_name, s.last_name, a.address 
FROM staff AS s
INNER JOIN address AS a
ON s.address_id = a.address_id;

-- 6b
SELECT s.first_name, s.last_name, SUM(p.amount)
FROM staff AS s
INNER JOIN payment AS p
ON s.staff_id = p.staff_id
WHERE MONTH(p.payment_date) = 08 AND YEAR(p.payment_date) = 2005
GROUP BY s.staff_id;

-- 6c
SELECT film.title, COUNT(film_actor.actor_id) AS 'actors'
FROM film
INNER JOIN film_actor
ON film.film_id = film_actor.film_id
GROUP BY film.title
ORDER BY actors DESC;

-- 6d
SELECT film.title, COUNT(inventory.inventory_id)
FROM film
INNER JOIN inventory
ON film.film_id = inventory.film_id
WHERE film.title = 'HUNCHBACK IMPOSSIBLE'
GROUP BY film.title;

-- 6e
SELECT customer.first_name, customer.last_name, SUM(payment.amount)
FROM payment
JOIN customer
ON customer.customer_id = payment.customer_id
GROUP BY payment.customer_id
ORDER BY customer.last_name;


-- 7a
SELECT title 
FROM film
WHERE title LIKE 'K%' 
OR title LIKE 'Q%'
AND language_id IN

(
	SELECT language_id 
	FROM language
	WHERE name = 'English'
);

-- 7b
SELECT first_name, last_name
FROM actor
WHERE actor_id IN
(	
	SELECT actor_id
    FROM film_actor
    WHERE film_id =
(	SELECT film_id
	FROM film
    WHERE title = 'Alone Trip'
 )
);

-- 7c I have no idea.

-- 7d Wait. What?!
SELECT title
FROM film
WHERE
(
	SELECT category_id
    FROM film_category
    WHERE
(
	SELECT film_id
    FROM film_category
    WHERE
(
	SELECT name
	FROM category
	WHERE name = 'Family'
  )
 )
);
